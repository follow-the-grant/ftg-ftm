# follow the grant x follow the money

scripts & resources for turning [pubmed
metadata](https://www.ncbi.nlm.nih.gov/pmc/tools/ftp/) into a
[followthemoney](https://docs.alephdata.org/developers/followthemoney) model.

## why?

it allows to model the schema in an easy way, and the [followthemoney
cli](http://github.com/alephdata/followthemoney/) together with
[followthemoney-store](https://github.com/alephdata/followthemoney-store)
allows easy data pipelines for input, output and transforming in different
formats, always being agnostic about the underlaying backend & infrastructure.

with this scripts collection it is easy to quickly have a look at the pubmed
dataset, export to csv for further analysis or import into an
[aleph](https://docs.alephdata.org/) database instance – everything in an easy,
reproducible way with existing toolchains.

## model

Articles: `Document` with the abstract text in property `summary`

*Authors*: `Person`

*Institutions*: `Organization`

*Authorship*: `Documentation` with `role` = 'author' between `Person` and `Document`

*Affiliations*: `Membership` with `role` = 'affiliated with' from `Person` in `Organization`

*Conflict of interest statement*: `Documentation` between `Person` and `Document`
with `role` = 'conflict of interest statement' and the statement in the
property `summary`

*Publisher*: `LegalEntity` with `Documentation` from `Document` via `role` = 'publisher'

## data

obtain pubmed source xml data from here: ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/

the `extract_pubmed.py` script expects the pdf csv to be able to set
`sourceUrl` properties to the `Document` entities:

        ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/oa_non_comm_use_pdf.csv

## requirements

python3

        pip install -r requirements.txt

it uses the [pubmed parser](https://titipata.github.io/pubmed_parser/) with coi-extractment included.

for parallel bash processing, use [GNU parallel](https://www.gnu.org/software/parallel/)

for easy bash csv parsing / processing, use [xsv](https://github.com/BurntSushi/xsv#installation)

## convert pubmed metadata to ftm

the magic happens in the script `extract_pubmed.py` – it takes pubmed xml file
paths as `stdin`, creates ftm entities and prints their json strings line per
line to `stdout`. that allows easy piping for different usecases within or
extending the ftm toolchain.

### generate ftm entities

for a small amount of xml files, export them into a json file:

        find ./path_to_pubmed/ -type f -name "*xml" | python3 extract_pubmed.py | ftm aggregate | ftm validate > pubmed_ftm.jsonl

for the full dataset, use `ftm store` (sqlite works as backend but postgres is
faster, as it allows concurrent writing via `parallel` – please see the
documentation of [`followthemoney-store`](https://github.com/alephdata/followthemoney-store)
on how to setup env vars for the sql backend):

        find ./path_to_pubmed/ -type f -name "*xml" | parallel -j8 --pipe python3 extract_pubmed.py oa_non_comm_use_pdf.csv | parallel -j8 --pipe ftm store write -d ftg

with `-j8` you set the number of parallel jobs, this should be the numbers of
cores of the machine for best performance.

once the entities are loaded into the ftm store, they can be exported to csv
(into a folder called "csv" in this example):

        ftm store iterate -d ftg | ftm export-csv -o csv

or written into an aleph instance:

        ftm store iterate -d ftg | alephclient write-entities -f ftg


more infos about how to use `ftm` and `alephclient`:

        ftm --help
        alephclient --help

*and their project repos on github...*

## analysis

### basic metadata analysis

https://docs.google.com/spreadsheets/d/1mBRClFgu2AHZ-Jwz659LKJdX5id7mAzbEy7LpuRo7oU/edit#gid=1787773084

start notebooks:

        cd ./analysis/
        jupyter notebook

The following steps are necessary to compute the
[./analysis/analyze-meta.ipynb](./analysis/analyze-meta.ipynb) ("meta analysis
/ overview statistics")
Then run the meta notebook as it relies on precomputed (aggregated) data.

1. annotate coi statements: **coi annotation notebook** [./analysis/annotate-coi.ipynb](./analysis/annotate-coi.ipynb)
2. annotate topics to documents - see [generate topics](#generate-topics)


The meta notebook exports some aggregated data to the folder
[./analysis/export/](./analysis/export/) – you can find this data here:
https://docs.google.com/spreadsheets/d/1mBRClFgu2AHZ-Jwz659LKJdX5id7mAzbEy7LpuRo7oU/edit#gid=0

#### other analysis

- **red flag**: [./analysis/red-flag.ipynb](./analysis/red-flag.ipynb)


### work with full dataset

to reduce the full data created with the commands above to only the metadata
you need for the analysis, use the tool
[xsv](https://github.com/BurntSushi/xsv#installation) to process the csvs:

**document metadata**

        xsv select id,publisher,publishedAt csv/Document.csv

**conflict of interest statements**

        xsv search -s role "conflict of interest statement" csv/UnknownLink.csv | xsv select id,subject,object,publisher,summary

### generate topics

        xsv select id,keywords meta/Documents.meta.csv | parallel --pipe python3 get_topics.py > meta/documents.topics.csv

Then run the notebook
[./analysis/cleanup-topics.ipynb](./analysis/cleanup-topics.ipynb) to generate
a cleaned list of the most common topics (this notebook could be improved :))

After it the notebook [./analysis/annotate-topics.ipynb](./analysis/annotate-topics.ipynb) to assign
the cleaned topics to each document.
