import click
import countrytagger
import csv
import json
import fingerprints
import logging
import os
import pubmed_parser as pp
import re
import requests
import sys
from datetime import datetime
from alephclient.api import AlephAPI
from followthemoney import model
from pathlib import Path
from urllib.parse import urljoin
from tempfile import NamedTemporaryFile

from split_coi import split_coi
from get_fulltext_coi import extract_coi_from_fulltext

_ = lambda x: fingerprints.generate(x)  # noqa

FTP_BASE = 'https://ftp.ncbi.nlm.nih.gov/pub/pmc/'

log = logging.getLogger()
log.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stderr)
log.addHandler(handler)


def init_aleph():
    api = AlephAPI()
    foreign_id = os.getenv('ALEPH_COLLECTION', 'ftg')
    collection = api.load_collection_by_foreign_id(foreign_id)
    collection_id = collection.get('id')
    return api, collection_id


def upload_document(data, url, api, collection_id):
    log.info(f'Downloading `{url}`')
    with requests.get(url, stream=True) as res:
        res.raise_for_status()
        with NamedTemporaryFile(delete=False) as f:
            for chunk in res.iter_content():
                f.write(chunk)
        metadata = {
            'file_name': f"{data['full_title'][:200]}.pdf",
            'title': data['full_title'],
            'summary': data['abstract'],
            # 'published_at': data['publication_date'],
            # 'publisher': data['journal'],
            'source_url': url,
            'keywords': data['subjects'].split(';'),
            # 'author': ['{} {}'.format(*reversed(a)) for a in data['author_list']]
        }
        doc = api.ingest_upload(collection_id, Path(f.name), metadata)
        os.unlink(f.name)
    return doc.get('id')


# pmid has precedence as identifier to assign grants later
IDS = ('pmid', 'pmc', 'doi')


def _get_doc_id(data):
    for key in IDS:
        if data.get(key):
            return key, data[key]


def make_entities(data, fpath, document_id=None, source_url=None):
    authors = data.get('author_list', [])

    if document_id is None:
        doc_id = _get_doc_id(data)
        if not doc_id:
            return []

        publication_year = data['publication_year']
        try:
            publication_date = datetime.strptime(data['publication_date'], '%d-%m-%Y')
        except ValueError:
            publication_date = publication_year

        document = model.make_entity('Document')
        document.make_id(*doc_id)
        # document.add('foreign_id', data['pmid'])
        document.add('title', data['full_title'])
        document.add('summary', data['abstract'])
        document.add('publishedAt', publication_date)
        document.add('publisher', data['journal'])
        document.add('author', ['{} {}'.format(*reversed(a[:2])) for a in authors])
        kwds = data.get('keywords', '').split(';')
        if kwds:
            document.add('keywords', kwds)
        else:
            document.add('keywords', data['subjects'].split(';'))
        if source_url:
            document.add('sourceUrl', source_url)
        document_id = document.id
        yield document.to_dict()

        publisher = model.make_entity('LegalEntity')
        publisher.make_id(_(data['journal']))
        publisher.add('name', data['journal'])
        yield publisher.to_dict()

        # link article -> publisher
        published = model.make_entity('Documentation')
        published.make_id(publisher.id, document.id)
        published.add('entity', publisher)
        published.add('document', document)
        published.add('role', 'publisher')
        published.add('date', publication_date)
        yield published.to_dict()

    affiliates = {}
    for key, name in dict(data.get('affiliation_list', [])).items():
        affiliate = model.make_entity('Organization')
        # move cleaning to another module?
        name = re.sub(r'^[Xgrid\.\d\s]*', '', name)
        nname = _(name)
        affiliate.make_id(nname)
        affiliate.add('name', name)
        if affiliate.id:
            countries = sorted(countrytagger.tag_text_countries(nname), key=lambda x: x[1])
            if countries:
                affiliate.add('country', countries[-1][2])
            yield affiliate.to_dict()
            affiliates[key] = affiliate

    author_affiliations = {tuple(a[:2]): [] for a in authors}
    for a in authors:
        author_affiliations[tuple(a[:2])].append(affiliates.get(a[2]))

    coi = None
    if not data['coi_statement']:
        # try to extract from fulltext
        coi_statement = extract_coi_from_fulltext(fpath) or ''
        if len(coi_statement) > 22:
            data['coi_statement'] = coi_statement

    if data['coi_statement']:
        # the full coi statement
        coi = model.make_entity('PlainText')
        coi.make_id('coi', document_id)
        coi.add('title', 'conflict of interest statement (article metadata)')
        coi.add('bodyText', data['coi_statement'])
        coi.add('publisher', data['journal'])
        coi.add('date', publication_date)
        coi.add('parent', document_id)

        # link article -> coi statement
        coi_ref = model.make_entity('Documentation')
        coi_ref.make_id('coi_ref', document_id)
        coi_ref.add('date', publication_date)
        coi_ref.add('publisher', data['journal'])
        coi_ref.add('document', document)
        coi_ref.add('entity', coi)
        coi_ref.add('role', 'conflict of interest statement')
        coi_ref.add('summary', data['coi_statement'])
        yield coi_ref.to_dict()

        # coi splitting for authors
        coi_authors = [tuple(reversed(a[:2])) for a in authors]
        coi_statements = {a if a == 'all' else ' '.join(a): statements
                          for a, statements in split_coi(data['coi_statement'], coi_authors)}

    for last_name, first_name, affiliate_key in authors:
        author = model.make_entity('Person')
        author_name = f'{first_name} {last_name}'
        # FIXME deduplication currently:
        # make id based on fingerprinted name and all affiliations found in this paper
        author.make_id(_(author_name),
                       *sorted(a.id for a in author_affiliations[(last_name, first_name)]))
        author.add('name', f'{first_name} {last_name}')
        author.add('firstName', first_name)
        author.add('lastName', last_name)

        if author.id:
            if coi is not None:
                coi.add('author', author.get('name'))

                # link author -> full coi statement
                author_coi_ref = model.make_entity('Documentation')
                author_coi_ref.make_id('author_coi_ref', document_id, author.id)
                author_coi_ref.add('date', publication_date)
                author_coi_ref.add('document', coi)
                author_coi_ref.add('entity', author)
                author_coi_ref.add('publisher', data['journal'])
                author_coi_ref.add('role', 'conflict of interest statement')
                author_coi_ref.add('summary', data['coi_statement'])
                yield author_coi_ref.to_dict()

                for identifier in (author_name, 'all'):
                    if identifier in coi_statements:
                        # individual coi statement for author
                        author_coi = model.make_entity('PlainText')
                        author_coi.make_id('author_coi', document_id, author.id, identifier)
                        author_coi.add('title', f'individual conflict of interest statement ({author_name})')
                        author_coi.add('bodyText', coi_statements[identifier])
                        author_coi.add('publisher', data['journal'])
                        author_coi.add('date', publication_date)
                        author_coi.add('author', author_name)
                        yield author_coi.to_dict()

                        # link author -> individual coi statement
                        author_coi_ref = model.make_entity('Documentation')
                        author_coi_ref.make_id('author_coi_ref', document_id, author.id, author_coi.id)
                        author_coi_ref.add('date', publication_date)
                        author_coi_ref.add('publisher', data['journal'])
                        author_coi_ref.add('document', author_coi)
                        author_coi_ref.add('entity', author)
                        author_coi_ref.add('role', f'individual conflict of interest statement')
                        author_coi_ref.add('summary', coi_statements[identifier])
                        yield author_coi_ref.to_dict()

                        # link individual coi statement -> article
                        author_coi_article_ref = model.make_entity('Documentation')
                        author_coi_article_ref.make_id('author_coi_article_ref', document_id, author.id, author_coi.id)
                        author_coi_article_ref.add('date', publication_date)
                        author_coi_article_ref.add('publisher', data['journal'])
                        author_coi_article_ref.add('document', document)
                        author_coi_article_ref.add('entity', author_coi)
                        author_coi_article_ref.add('role', f'individual conflict of interest statement')
                        author_coi_article_ref.add('summary', coi_statements[identifier])
                        yield author_coi_article_ref.to_dict()

            # link author -> article
            authorship = model.make_entity('Documentation')
            authorship.make_id('authorship', author.id, document_id)
            authorship.add('entity', author)
            authorship.add('document', document_id)
            authorship.add('date', publication_date)
            authorship.add('role', 'author')
            yield authorship.to_dict()

            # link author -> institutions
            affiliate = affiliates.get(affiliate_key)
            if affiliate:
                affiliated = model.make_entity('Membership')
                affiliated.make_id('affiliation', author.id, affiliate.id)
                affiliated.add('member', author)
                affiliated.add('organization', affiliate)
                affiliated.add('date', publication_year)
                affiliated.add('role', 'affiliated with')
                affiliated.add('publisher', data['journal'])
                yield affiliated.to_dict()

                for country in affiliate.countries:
                    author.add('country', country)
            yield author.to_dict()

    if coi is not None:
        yield coi.to_dict()


@click.command()
@click.argument('pdf_index')
@click.option('--download', help='Download & index source pdfs')
def extract(pdf_index, download):
    with open(pdf_index) as f:
        reader = csv.DictReader(f)
        pdfs = {r['PMID']: r['File'] for r in reader}
    if download:
        aleph = init_aleph()
    for fpath in sys.stdin:
        fpath = fpath.strip()
        try:
            data = pp.parse_pubmed_xml(fpath)
            document_id = None
            source_url = None
            if data['pmid'] and data['pmid'] in pdfs:
                source_url = urljoin(FTP_BASE, pdfs[data['pmid']])
                if download:
                    document_id = upload_document(data, source_url, *aleph)
                    with open('aleph_documents.csv', 'a') as f:
                        f.write(','.join((document_id, data['pmid'])) + '\n')
            for entity in make_entities(data, fpath, document_id, source_url):
                sys.stdout.write(json.dumps(entity) + '\n')
        except Exception as e:
            log.error(f'Error: `{e.__class__.__name__}`: "{e}"')


if __name__ == '__main__':
    extract()
