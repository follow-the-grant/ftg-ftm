import sys
from normality import normalize


STOPWORDS = (
    'report',
    'article',
    'research',
    'editor',
    'review',
    'original',
    'abstract',
    'commentary',
    'presentation',
    'correspondence',
    'letter',
    'study',
    'communication',
    'protocol',
    'dispatch',
    'erratum',
    'news',
    'perspective',
    'correction',
    'full paper',
    'issue',
    'methods online',
    'corrigendum',
    'general',
    'note'
)


def is_topic(value):
    for word in STOPWORDS:
        if word in value:
            return False
    try:
        int(value)
        return False
    except ValueError:
        pass
    return True


def get_keywords(row):
    if row and isinstance(row, str):
        try:
            id_, keywords = row.split(',', 1)
            for keyword in keywords.split(';'):
                norm_kwd = normalize(keyword)
                if norm_kwd and is_topic(norm_kwd):
                    yield id_, norm_kwd
        except ValueError:
            pass


if __name__ == '__main__':
    for line in sys.stdin.readlines():
        for row in get_keywords(line):
            sys.stdout.write(','.join(row) + '\n')
